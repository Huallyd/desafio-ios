//
//  RepositoryService.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
import ObjectMapper

struct RepositoryService {    
    static func getListRepositories (page: Int, completionHandler: @escaping (_ success: Bool, _ msg: String, _ repositories: [Repository]?) -> ()) {
        
        var repositories = [Repository]()
        let url = UrlServer.repositories+"\(page)"
        
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { (response: DataResponse<Any>) in
            switch response.result {
                
            case .success:
                let data = response.result.value as! Dictionary<String, AnyObject>
                switch response.response!.statusCode {
                case 200:
                    if let results = data[GitUrlKeys.items] as? Array<[String:AnyObject]> {
                        for result in results {
                            if let reposity = Mapper<Repository>().map(JSON: result){
                                repositories.append(reposity)
                            }
                        }
                    }
                    completionHandler(true, FeedbackRequestMessages.success, repositories)
                default:
                    completionHandler(true, FeedbackRequestMessages.fail, nil)
                }
            case .failure(_):
                completionHandler(false, FeedbackRequestMessages.networkError, repositories)
            }
        }
    }
    
    static func getImageBy(imageView: UIImageView, url: String, completionHandler: @escaping (_ success: Bool, _ msg: String) -> ()) {
        imageView.setIndicatorStyle(.gray)
        imageView.setShowActivityIndicator(true)
        imageView.sd_setImage(with: URL(string: url), completed: { (image, error, type, url) in
            if error == nil {
                completionHandler(true, "success")
            } else {
                completionHandler(false, error!.localizedDescription)
            }
        })
    }
}
