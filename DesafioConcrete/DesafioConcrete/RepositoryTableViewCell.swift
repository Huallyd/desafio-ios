//
//  RepositoryTableViewCell.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {

    static var identifier: String {
        return "repositoryCell"
    }
    
    static var cellHeight: CGFloat {
        return 140.0
    }
    
    static var nibName: String {
        return "RepositoryTableViewCell"
    }
    
    @IBOutlet weak var repositoryNameLabel: UILabel!
    @IBOutlet weak var repositoryDescriptionLabel: UILabel!
    @IBOutlet weak var forkAmountLabel: UILabel!
    @IBOutlet weak var evaluationAmountLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    var presenter: RepositoryCellPresenter = RepositoryCellPresenter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViewDelegate()
    }
    
    func setupViewDelegate(){
        presenter.setViewDelegate(delegate: self)
    }
}

extension RepositoryTableViewCell: ViewCellDelegate {
    
    func setupCell() {
        repositoryNameLabel.text = presenter.repository.name
        repositoryDescriptionLabel.text = presenter.repository.repositoryDescription
        forkAmountLabel.text = presenter.repository.forkCount!.description
        evaluationAmountLabel.text = presenter.repository.starCount!.description
        userNameLabel.text = presenter.getAuthorOfRepository().name
        presenter.getUserPhoto(imageView: userImage)
    }
}
