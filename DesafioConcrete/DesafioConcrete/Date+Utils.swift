//
//  Date+Utils.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

extension Date {
    func getStringToDate(dateFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        let date = dateFormatter.string(from:self)
        return date
    }
}

extension String {
    func getDateToString(dateFormat: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat
        if let date = dateFormatter.date(from: self) {
            return date
        }
        return Date()
    }
    
    func getStringToDateFormatter(dateFormat: String) -> String {
        let date = self.getDateToString(dateFormat: dateFormat)
        return date.getStringToDate(dateFormat: dateFormat)
    }
}
