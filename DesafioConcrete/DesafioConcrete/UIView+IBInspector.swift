//
//  UIView+IBInspector.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

extension UIView {
    @IBInspectable var roundedCircle: Bool {
        get {
            return self.layer.cornerRadius == self.frame.height / 2
        }
        set {
            if newValue == true {
                self.layer.cornerRadius = self.frame.height / 2
                self.layer.masksToBounds = true
            } else {
                self.layer.cornerRadius = 0
            }
        }
    }
}
