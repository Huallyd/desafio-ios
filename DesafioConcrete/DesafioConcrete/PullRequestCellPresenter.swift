//
//  PullRequestCellPresenter.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

struct PullRequestCellPresenter {
    
    var delegate: ViewCellDelegate?
    
    var pullRequest: PullRequest = PullRequest() {
        didSet{
            delegate?.setupCell()
        }
    }
    
    mutating func setViewDelegate(delegate: ViewCellDelegate) {
        self.delegate = delegate
    }
    
    func getAuthorOfPullRequest() -> Author {
        return pullRequest.author!
    }
    
    func getUserPhoto(imageView: UIImageView){
        let url = getAuthorOfPullRequest().urlPhoto
        PullRequestService.getImageBy(imageView: imageView, url: url!) { (success, msg) in
            if success {
            } else {}
        }
    }
}
