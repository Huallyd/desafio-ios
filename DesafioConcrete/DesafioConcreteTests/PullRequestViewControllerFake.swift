//
//  PullRequestViewControllerFake.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 29/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

@testable import DesafioConcrete

class PullRequestViewControllerFake: PullRequestViewController {
    
    var presenterFake = PullRequestPresenterFake()
    override func openBrowser(url: String) {
        presenterFake.urlPullRequest = url;
    }
    
}


