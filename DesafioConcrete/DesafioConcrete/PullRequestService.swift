//
//  PullRequestService.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

struct PullRequestService {
    
    static func getPullRequests(repository: Repository,  completionHandler: @escaping (_ success: Bool, _ msg: String) -> ()) {
        
        let url = getPullRequestUrl(authorName: repository.author!.name!, repositoryName: repository.name!)
        
        Alamofire.request(url, method: .get, parameters: nil).responseJSON { (response: DataResponse<Any>) in
            switch response.result {
                
            case .success:
                let results = response.result.value as! Array<[String:AnyObject]>
                switch response.response!.statusCode {
                case 200:
                    results.forEach {
                        if let pullRequest = Mapper<PullRequest>().map(JSON: $0) {
                            repository.setPullRequest(pullRequest: pullRequest)
                        }
                    }
                    completionHandler(true, FeedbackRequestMessages.success)
                default:
                    completionHandler(false, FeedbackRequestMessages.fail)
                }
            case .failure(_):
                completionHandler(false, FeedbackRequestMessages.networkError)
                
            }
        }
    }
    
    static func getPullRequestUrl(authorName: String, repositoryName: String) -> String {
        return UrlServer.pullRequests+"\(authorName)/\(repositoryName)\(UrlServer.completionPullRequests)"
    }
    
    static func getImageBy(imageView: UIImageView, url: String, completionHandler: @escaping (_ success: Bool, _ msg: String) -> ()) {
        
        RepositoryService.getImageBy(imageView: imageView, url: url) { (success, msg) in
            completionHandler(success, msg)
        }
    }
}
