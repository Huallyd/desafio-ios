//
//  PullRequestPresenter.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import SafariServices

protocol PullRequestDelegate {
    func openBrowser(url: String)
    var statesPullRequest: NSMutableAttributedString? {get set}
}

class PullRequestPresenter: NSObject {
    
    var pullRequests: [PullRequest] = [PullRequest]()
    var pullRequestOpened: Int?
    var pullRequestClosed: Int?
    
    var delegate: PullRequestDelegate?
    
    func setViewDelegate(delegate: PullRequestDelegate) {
        self.delegate = delegate
    }
    
    func getUrlBrowser(row: Int) {
        delegate?.openBrowser(url: pullRequests[row].url!)
    }
    
    func getStatesPullRequest() {
        let opened = getCustomAttributedString(text: pullRequestOpened!.description+" opened ", color: UIColor.colorWithHexString("DF9300"))
        
        let closed = getCustomAttributedString(text: "/ "+pullRequestClosed!.description + " closed", color: UIColor.black)
        
        opened.append(closed)
        delegate?.statesPullRequest = opened
    }

    func getCustomAttributedString(text: String, color: UIColor) -> NSMutableAttributedString {
        
        return NSMutableAttributedString(string: text, attributes: [NSForegroundColorAttributeName: color])
    }
}
