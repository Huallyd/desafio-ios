//
//  PullRequest.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import ObjectMapper

enum PullRequestState {
    case open
    case close
}

class PullRequest: NSObject, Mappable {
    
    var title: String?
    var body: String?
    var date: String?
    var author: Author?
    var state: String?
    var url: String?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        title <- map[PullRequestUrlKeys.title]
        body <- map[PullRequestUrlKeys.body]
        date <- map[PullRequestUrlKeys.createdDate]
        author <- map[PullRequestUrlKeys.author]
        state <- map[PullRequestUrlKeys.state]
        url <- map[PullRequestUrlKeys.url]
    }
}
