//
//  PullRequestTableViewCell.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class PullRequestTableViewCell: UITableViewCell {

    static var identifier: String {
        return "pullRequestCell"
    }
    
    static var cellHeight: CGFloat {
        return 160.0
    }
    
    static var nibName: String {
        return "PullRequestTableViewCell"
    }
    
    @IBOutlet weak var titlePullRequestLabel: UILabel!
    @IBOutlet weak var bodyPullRequestLabel: UILabel!
    @IBOutlet weak var datePullLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    var presenter: PullRequestCellPresenter = PullRequestCellPresenter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupViewDelegate()
    }
    
    func setupViewDelegate(){
        presenter.setViewDelegate(delegate: self)
    }
}

extension PullRequestTableViewCell: ViewCellDelegate {
    
    func setupCell() {
        titlePullRequestLabel.text = presenter.pullRequest.title
        bodyPullRequestLabel.text = presenter.pullRequest.body
        userNameLabel.text = presenter.getAuthorOfPullRequest().name
        datePullLabel.text = presenter.pullRequest.date
        presenter.getUserPhoto(imageView: userImage)
    }
}

