//
//  RepositoryViewControllerFake.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 29/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
@testable import DesafioConcrete

class RepositoryViewControllerFake: RepositoryViewController {
    
    var presenterFake = RepositoryPresenterFake()
    
    override func loadingView() {
        presenterFake.isCallLoadingView = true
    }
    
    override func unloadingView() {
        presenterFake.isCallUnloadingView = true
    }
    
    override func loadingIndicatorTableViewFooter() {
        presenterFake.isCallLoadingIndicatorTableViewFooter = true;
    }
    
}
