//
//  RepositoryPresenter.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

enum OperatorType {
    case add
    case decrease
}

protocol RepositoryDelegate {
    func reload()
    func pushFor(segueIdentifier: String)
    func loadingView()
    func unloadingView()
    func loadingIndicatorTableViewFooter()
    func unloadingIndicatorTableViewFooter()
    func showMessage(title: String, msg: String)
}

class RepositoryPresenter: NSObject {
    
    var repositories: [Repository] = [Repository]()
    var repository: Repository = Repository()
    
    var delegate: RepositoryDelegate?
    var page: Int = 1
    
    func setViewDelegate(delegate: RepositoryDelegate) {
        self.delegate = delegate
    }
    
    func getRepositories(){
        setupLoadingIndicator()
        RepositoryService.getListRepositories(page: page) { (success, msg, repositories) in
            if success {
                repositories!.forEach {
                    self.repositories.append($0)
                }
                self.delegate?.reload()
            } else {
                self.updatePage(operatorType: .decrease)
            }
            self.removeLoadingIndicator()
        }
        
        updatePage(operatorType: .add)
    }
    
    func updatePage(operatorType: OperatorType) {
        page = operatorType == .add ? page+1 : page-1
    }
    
    func getPullRequest(row: Int) {
        if repositories[row].pullRequests.count < 1 {
            delegate?.loadingView()
            PullRequestService.getPullRequests(repository: repositories[row]) { (success, msg) in
                if success {
                    self.repository = self.repositories[row]
                    self.delegate?.pushFor(segueIdentifier: SegueIdentifiers.repositoryToPullRequest)
                } else {
                    self.delegate?.showMessage(title: "error", msg: msg)
                }
                self.delegate?.unloadingView()
            }
        } else {
            self.delegate?.pushFor(segueIdentifier: SegueIdentifiers.repositoryToPullRequest)
        }
    }
    
    func setupLoadingIndicator() {
        if page != 1 {
            delegate?.loadingIndicatorTableViewFooter()
        } else {
            delegate?.loadingView()
        }
    }
    
    func removeLoadingIndicator() {
        if page <= 2 {
            delegate?.unloadingView()
        } else {
            delegate?.unloadingIndicatorTableViewFooter()
        }
    }
}
