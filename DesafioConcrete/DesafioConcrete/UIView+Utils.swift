//
//  UIView+Utils.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

extension UIView{
    func addLoadingFeedback(_ duration: TimeInterval = 0.2, _ backgroundColor: UIColor = UIColor(white: 0.0, alpha: 0.75), _ indicatorStyle: UIActivityIndicatorViewStyle = .white, _ alpha: CGFloat = 0.0) {
        guard viewWithTag(10) == nil else { return }
        let lockView = UIView(frame: bounds)
        lockView.backgroundColor = backgroundColor
        lockView.tag = 10
        lockView.alpha = alpha
        let activity = UIActivityIndicatorView(activityIndicatorStyle: indicatorStyle)
        activity.hidesWhenStopped = true
        
        activity.center = lockView.center
        
        activity.translatesAutoresizingMaskIntoConstraints = false
        
        lockView.addSubview(activity)
        activity.startAnimating()
        
        self.addSubview(lockView)
        
        let xCenterConstraint = NSLayoutConstraint(item: activity, attribute: .centerX, relatedBy: .equal, toItem: lockView, attribute: .centerX, multiplier: 1, constant: 0)
        
        let yCenterConstraint = NSLayoutConstraint(item: activity, attribute: .centerY, relatedBy: .equal, toItem: lockView, attribute: .centerY, multiplier: 1, constant: 0)
        
        NSLayoutConstraint.activate([xCenterConstraint, yCenterConstraint])
        UIView.animate(withDuration: 0.2, animations: {
            lockView.alpha = 1.0
        })
        
    }
    
    func removeLoadingFeedback() {
        if let lockView = self.viewWithTag(10) {
            UIView.animate(withDuration: 0.2, animations: {
                lockView.alpha = 0.0
            }, completion: { finished in
                lockView.removeFromSuperview()
            })
        }
    }
}
