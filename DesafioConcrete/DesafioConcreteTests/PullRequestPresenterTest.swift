//
//  PullRequestPresenterTest.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 29/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//


import Nimble
import Quick
@testable import DesafioConcrete

class PullRequestPresenterTest: QuickSpec {
    
    override func spec() {
        
        describe("Pull Request presenter test") {
            let presenter = PullRequestPresenterFake()
            
            beforeEach {
                let controller = PullRequestViewControllerFake()
                controller.presenterFake = presenter
                presenter.delegate = controller
            }
            
            it("no should to be nil presenter", closure: {
                expect(presenter).notTo(beNil())
            })
            
            it("should have a presenter of type", closure: {
                expect(presenter).to(beAKindOf(NSObject.self))
            })
            
            it("Should the method delegate openUrlBrowser", closure: {
                presenter.getUrlBrowser(row: 0)
                expect(presenter.urlPullRequest).to(equal("https://www.concretesolutions.com.br/"))
            })
            
        }
    }
    
}
