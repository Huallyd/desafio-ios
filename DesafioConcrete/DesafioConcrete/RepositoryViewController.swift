//
//  RepositoryViewController.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class RepositoryViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var presenter = RepositoryPresenter()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let indexPathRow = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPathRow, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
        setupViewDelegate()
        getRepositories()
        tableView.tableFooterView = refreshIndicatorInTableViewFooter()
    }
    
    func refreshIndicatorInTableViewFooter() -> UIView {
        let viewIndicator = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 40))
        viewIndicator.backgroundColor = UIColor.white
        return viewIndicator
    }
    
    func getRepositories() {
        presenter.getRepositories()
    }
    
    func setupViewDelegate() {
        presenter.setViewDelegate(delegate: self)
    }
    
    func registerNib() {
        tableView.registerNibFrom(RepositoryTableViewCell.self)
    }
    
    func generateRepositoryCell (_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RepositoryTableViewCell.identifier, for: indexPath) as! RepositoryTableViewCell
        
        cell.presenter.repository = presenter.repositories[indexPath.row]
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? PullRequestViewController {
            let indexPath = tableView.indexPathForSelectedRow
            let repository = presenter.repositories[indexPath!.row]
            controller.presenter.pullRequests = repository.pullRequests
            controller.presenter.pullRequestOpened = repository.getPullRequestesStateAmount(state: .open)
            controller.presenter.pullRequestClosed = repository.getPullRequestesStateAmount(state: .close)
            controller.title = repository.name
        }
    }
}

extension RepositoryViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return generateRepositoryCell(tableView, cellForRowAt: indexPath)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.repositories.count
    }
}

extension RepositoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return RepositoryTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.getPullRequest(row: indexPath.row)
    }
}

extension RepositoryViewController: UIScrollViewDelegate {
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height {
            presenter.getRepositories()
        }
    }
}

extension RepositoryViewController: RepositoryDelegate {
    
    func reload() {
        tableView.reloadData()
    }
    
    func showMessage(title: String, msg: String) {
        self.present(ViewUtil.getAlertController(title: title, withMessage: msg), animated: true, completion: nil)
    }
    
    func loadingView() {
        self.view.addLoadingFeedback()
    }
    
    func unloadingView() {
        self.view.removeLoadingFeedback()
    }
    
    func loadingIndicatorTableViewFooter() {
        tableView.tableFooterView?.addLoadingFeedback(0.2, UIColor.white, UIActivityIndicatorViewStyle.gray, 1.0)
    }
    
    func unloadingIndicatorTableViewFooter() {
        tableView.tableFooterView?.removeLoadingFeedback()
    }
    
    func pushFor(segueIdentifier: String) {
        performSegue(withIdentifier: segueIdentifier, sender: self)
    }
}
