//
//  Author.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import ObjectMapper

class Author: NSObject, Mappable {
    
    var name: String?
    var urlPhoto: String?
    var photo: UIImage?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    
    func mapping(map: Map) {
        name <- map[OwnerUrlKeys.name]
        urlPhoto <- map[OwnerUrlKeys.photo]
    }
    
}
