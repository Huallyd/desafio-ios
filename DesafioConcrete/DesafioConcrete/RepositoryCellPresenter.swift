//
//  RepositoryCellPresenter.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

protocol ViewCellDelegate {
    func setupCell()
}

struct RepositoryCellPresenter {
    
    var delegate: ViewCellDelegate?
    
    var repository: Repository = Repository() {
        didSet {
            delegate?.setupCell()
        }
    }
    
    func getAuthorOfRepository() -> Author {
        return repository.author!
    }
    
    mutating func setViewDelegate(delegate: ViewCellDelegate) {
        self.delegate = delegate
    }
    
    func getUserPhoto(imageView: UIImageView){
        let url = getAuthorOfRepository().urlPhoto
        RepositoryService.getImageBy(imageView: imageView, url: url!) { (success, msg) in
            if success {
            } else {}
        }
    }
}
