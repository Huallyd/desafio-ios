//
//  RepositoryPresenterFake.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 29/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

import Foundation
@testable import DesafioConcrete

class RepositoryPresenterFake: RepositoryPresenter {
    
    var isCallLoadingView = false;
    var isCallUnloadingView = false;
    var isCallLoadingIndicatorTableViewFooter = false;
    
}
