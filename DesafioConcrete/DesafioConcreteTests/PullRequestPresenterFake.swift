//
//  PullRequestPresenterFake.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 29/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

@testable import DesafioConcrete

class PullRequestPresenterFake: PullRequestPresenter {
    
    let pullRequestO = "10"
    let pullRequestC = "5"
    var urlPullRequest: String!
    
    override init() {
        super.init()
        let pullRequest = PullRequest()
        pullRequest.title = "Additional rules ProGuard"
        pullRequest.body = "See http://squal Are rules"
        pullRequest.url = "https://www.concretesolutions.com.br/"
        self.pullRequests = [pullRequest]
    }
    
    override func getStatesPullRequest() {
        
        let opened = getCustomAttributedString(text: "/ "+pullRequestO.description + "  opened ", color: UIColor.colorWithHexString("DF9300"))
        
        let closed = getCustomAttributedString(text: "/ "+pullRequestC.description + " closed", color: UIColor.black)
        
        opened.append(closed)
        
    }
    
    override func getUrlBrowser(row: Int) {
        
    }
    
}
