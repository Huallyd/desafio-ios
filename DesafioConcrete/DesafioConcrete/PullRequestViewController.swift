//
//  PullRequestViewController.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import SafariServices

class PullRequestViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var statesLabel: UILabel!
    
    var webView: UIWebView!
    var presenter = PullRequestPresenter()
    
    var statesPullRequest: NSMutableAttributedString? {
        didSet {
            statesLabel.attributedText = statesPullRequest
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let indexPathRow = tableView.indexPathForSelectedRow {
            tableView.deselectRow(at: indexPathRow, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerNib()
        setupViewDelegate()
        getStatesPullRequest()
    }
    
    func setupViewDelegate() {
        presenter.setViewDelegate(delegate: self)
    }
    
    func getStatesPullRequest() {
        presenter.getStatesPullRequest()
    }
    
    func registerNib() {
        tableView.registerNibFrom(PullRequestTableViewCell.self)
    }
    
    func generateEmptyCell (_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = UITableViewCell(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width, height: 50))
    
        cell.textLabel?.text = PlaceholderMessages.emptyPullRequests
        cell.textLabel?.textAlignment = .center

        return cell
        
    }
    
    func generatePullRequestCell (_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: PullRequestTableViewCell.identifier, for: indexPath) as! PullRequestTableViewCell
        cell.presenter.pullRequest = presenter.pullRequests[indexPath.row]
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? WebViewController {
            let indexPath = tableView.indexPathForSelectedRow
            controller.presenter.url = presenter.pullRequests[indexPath!.row].url
        }
    }
}

extension PullRequestViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if presenter.pullRequests.count < 1 {
            return generateEmptyCell(tableView, cellForRowAt: indexPath)
        } else {
            return generatePullRequestCell(tableView, cellForRowAt: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if presenter.pullRequests.count < 1 {
            return 1
        } else {
            return presenter.pullRequests.count
        }
        
    }
}

extension PullRequestViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return PullRequestTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.getUrlBrowser(row: indexPath.row)
    }
}

extension PullRequestViewController: PullRequestDelegate, UIWebViewDelegate {
    
    func openBrowser(url: String) {
        if #available(iOS 9.0, *) {
            let svc = SFSafariViewController(url: URL(string: url)!)
            svc.delegate = self
            self.present(svc, animated: true, completion: nil)
        } else {
            performSegue(withIdentifier: SegueIdentifiers.pullRequestToWebView, sender: self)
        }
    }
}

extension PullRequestViewController : SFSafariViewControllerDelegate{
    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(_ controller: SFSafariViewController){
        controller.dismiss(animated: true, completion: nil)
    }
}
