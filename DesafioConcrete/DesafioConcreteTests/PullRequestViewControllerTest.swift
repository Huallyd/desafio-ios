//
//  PullRequestViewControllerTest.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 29/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import Nimble
import Quick

@testable import DesafioConcrete

class PullRequestViewControllerTest: QuickSpec {
    
    override func spec() {
        describe("Pull request view controller test") {
            context("Valid View", {
                var pullRequestViewController: PullRequestViewController!
                
                beforeEach {
                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    pullRequestViewController = storyboard.instantiateViewController(withIdentifier: "PullRequestViewController") as! PullRequestViewController
                    let pullRequestPresenterFake = PullRequestPresenterFake()
                    pullRequestPresenterFake.delegate = pullRequestViewController
                    pullRequestViewController.presenter = pullRequestPresenterFake
                    let _ = pullRequestViewController.view
                    
                }
                
                it("no should to be nil repositoryViewController", closure: {
                    expect(pullRequestViewController).notTo(beNil())
                })
                
                it("should have a pullRequestViewController of type", closure: {
                    expect(pullRequestViewController).to(beAKindOf(UIViewController.self))
                    expect(pullRequestViewController).notTo(beAKindOf(UITableViewController.self))
                })
                
                it("no should to be nil view tableView", closure: {
                    expect(pullRequestViewController.tableView).notTo(beNil())
                })
                
                it("no should to be nil view statesLabel", closure: {
                    expect(pullRequestViewController.statesLabel).notTo(beNil())
                })
                
                it("should contain 1 section", closure: {
                    expect(pullRequestViewController.tableView.numberOfSections).to(equal(1))
                })
                
                it("should contain text", closure: {
                    expect(pullRequestViewController.statesLabel.text).to(equal("/ 10  opened / 5 closed"))
                })
                
            })
        }
    }
}
