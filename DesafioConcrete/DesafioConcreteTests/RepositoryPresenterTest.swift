//
//  RepositoryPresenterTest.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 29/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import XCTest
import Nimble
import Quick
@testable import DesafioConcrete

class RepositoryPresenterTest: QuickSpec {
    
    override func spec() {
        describe("Repository presenter test") {
            
            var repositoryPresenter = RepositoryPresenterFake()
            
            beforeEach {
                repositoryPresenter = RepositoryPresenterFake()
                let repositoryViewControllerFake = RepositoryViewControllerFake()
                repositoryViewControllerFake.presenterFake = repositoryPresenter
                repositoryPresenter.delegate = repositoryViewControllerFake
            }
            
            it("no should to be nil repositoryPresenter", closure: {
                expect(repositoryPresenter).notTo(beNil())
            })
            
            it("should have a repositoryPresenter of type", closure: {
                expect(repositoryPresenter).to(beAKindOf(NSObject.self))
            })
            
            it("should page 1", closure: {
                expect(repositoryPresenter.page).to(equal(1))
            })
            
            it("shoudl update page add", closure: {
                repositoryPresenter.updatePage(operatorType: OperatorType.add)
                expect(repositoryPresenter.page).to(equal(2))
            })
            
            it("shoudl update page decres", closure: {
                repositoryPresenter.updatePage(operatorType: OperatorType.decrease)
                expect(repositoryPresenter.page).to(equal(0))
            })
            
            it("Should the method delegate loadingView", closure: {
                repositoryPresenter.setupLoadingIndicator()
                expect(repositoryPresenter.isCallLoadingView).to(equal(true))
            })
            
            it("Should the method delegate indicatorTableViewFooter", closure: {
                repositoryPresenter.page = 2
                repositoryPresenter.setupLoadingIndicator()
                expect(repositoryPresenter.isCallLoadingIndicatorTableViewFooter).to(equal(true))
            })
            
            it("Should the method delegate unloadingView", closure: {
                repositoryPresenter.removeLoadingIndicator()
                expect(repositoryPresenter.isCallUnloadingView).to(equal(true))
            })
            
            it("Should the method delegate loadingIndicatorTableViewFooter", closure: {
                repositoryPresenter.page = 3
                repositoryPresenter.setupLoadingIndicator()
                expect(repositoryPresenter.isCallLoadingIndicatorTableViewFooter).to(equal(true))
            })
            
            
        }
    }
}
