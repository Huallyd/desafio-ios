//
//  Constants.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//


//MARK - URL Server

struct UrlServer {
    static let server = "https://api.github.com/"
    static let repositories = "\(server)search/repositories?q=language:Java&sort=stars&page="
    static let pullRequests = "\(server)repos/"
    static let completionPullRequests = "/pulls"
}

//MARK - Git URL keys

struct GitUrlKeys {
    static let items = "items"
    static let id = "id"
    static let name = "name"
    static let description = "description"
    static let starCount = "stargazers_count"
    static let forksCount = "forks"
    static let avatarUrl = "avatar_url"
    static let authorName = "login"
    static let owner = "owner"
}

//MARK - Owner URL keys

struct OwnerUrlKeys {
    static let name = "login"
    static let photo = "avatar_url"
}

//MARK - Owner URL keys

struct PullRequestUrlKeys {
    static let title = "title"
    static let state = "state"
    static let body = "body"
    static let createdDate = "created_at"
    static let author = "user"
    static let url = "html_url"
}

//MARK - Segue Identifiers

struct SegueIdentifiers {
    static let repositoryToPullRequest = "goToPullRequest"
    static let pullRequestToWebView = "goToWebView"
}

//MARK - Date Formats

struct DateFormat {
    static let dmy = "dd/MM/YYYY"
}

//MARK - Placeholder Messages

struct PlaceholderMessages {
    static let emptyPullRequests = "Sem pull requests"
}

//MARK - Feedback Request Messages
struct FeedbackRequestMessages {
    static let networkError = "Network Error"
    static let success = "Success"
    static let fail = "Fail"
}
