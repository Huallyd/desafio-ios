//
//  RepositoryViewControllerTest.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//



import Quick
import Nimble

@testable import DesafioConcrete

class RepositoryViewControllerTest: QuickSpec {
    
    override func spec() {
        describe("Repository view controller test") {
            context("Valid View", {
                var repositoryViewController: RepositoryViewController!
                
                beforeEach {
                    let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
                    repositoryViewController = storyboard.instantiateViewController(withIdentifier: "RepositoryViewController") as! RepositoryViewController
                    let _ = repositoryViewController.view
                    let _ = UINavigationController(rootViewController: repositoryViewController)
                }
                
                it("no should to be nil repositoryViewController", closure: {
                    expect(repositoryViewController).notTo(beNil())
                })
                
                it("should have a repositoryViewController of type", closure: {
                    expect(repositoryViewController).to(beAKindOf(UIViewController.self))
                    expect(repositoryViewController).notTo(beAKindOf(UITableViewController.self))
                })
                
                it("no should to be nil view tableView", closure: {
                    expect(repositoryViewController.tableView).notTo(beNil())
                })
                
                it("should have the expected navigation title") {
                    expect(repositoryViewController.navigationItem.title).to(equal("Github JavaPop"))
                }
                
                it("should have the expected TableViewFooter backgound and height", closure: {
                    let view = repositoryViewController.refreshIndicatorInTableViewFooter()
                    expect(view.backgroundColor).to(equal(UIColor.white))
                    expect(view.frame.height).to(equal(40))
                    expect(view.frame.height).notTo(equal(30))
                })
                
            })
        }
    }
}
