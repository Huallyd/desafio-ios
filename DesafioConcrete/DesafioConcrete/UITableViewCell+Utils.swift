//
//  UITableViewCell+Utils.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

protocol UITableViewCellHelper: class {
    static var cellHeight: CGFloat { get }
    static var identifier: String { get }
    static var nibName: String { get }
}

extension UITableViewCellHelper {
    static var cellHeight: CGFloat {
        return 44.0
    }
    
    static var identifier: String {
        return ""
    }
    
    static var nibName: String {
        return ""
    }
}

extension UITableView {
    
    func registerNibFrom(_ cellClass: UITableViewCell.Type) {
        let nibName = cellClass.value(forKey: "nibName") as! String
        let cellIdentifier = cellClass.value(forKey: "identifier") as! String
        let nib = UINib(nibName: nibName, bundle: nil)
        self.register(nib, forCellReuseIdentifier: cellIdentifier)
    }
}
