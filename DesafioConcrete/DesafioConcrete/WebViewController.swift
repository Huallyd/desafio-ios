//
//  WebViewController.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit

class WebViewController: UIViewController, UIWebViewDelegate {
    
    var presenter: WebPresenter = WebPresenter()
    
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        requestUrl()
    }
    
    func requestUrl() {
        if let url = URL(string: presenter.url!) {
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }
}
