//
//  Repository.swift
//  DesafioConcrete
//
//  Created by Huallyd Smadi on 28/05/17.
//  Copyright © 2017 Huallyd. All rights reserved.
//

import UIKit
import ObjectMapper

class Repository: NSObject, Mappable {
    
    var name: String?
    var author: Author?
    var pullRequests: [PullRequest] = [PullRequest]()
    var repositoryDescription: String?
    var starCount: Int?
    var forkCount: Int?
    
    override init() {
        super.init()
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    func setPullRequest (pullRequest: PullRequest){
        pullRequests.append(pullRequest)
    }
    
    func getPullRequestesStateAmount(state: PullRequestState) -> Int {
        let stateType: String = state == .close ? "close" : "open"
        var amountState: Int = 0
        for pullRequest in pullRequests where pullRequest.state == stateType {
            amountState = amountState+1
        }
        return amountState
    }
    
    func mapping(map: Map) {
        name <- map[GitUrlKeys.name]
        author <- map[GitUrlKeys.owner]
        repositoryDescription <- map[GitUrlKeys.description]
        starCount <- map[GitUrlKeys.starCount]
        forkCount <- map[GitUrlKeys.forksCount]
    }
}
